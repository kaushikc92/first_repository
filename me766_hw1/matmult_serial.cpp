#include <iostream>
using namespace std;


#define N 3


int main()
{

	float a[N][N];
	float b[N][N];
	float c[N][N];

	// Initialize buffers.
	for (int i = 0; i < N; ++i) 
	{
		for (int j = 0; j < N; ++j) 
		{
			a[i][j] = (float)i + j;
			b[i][j] = (float)i - j;
			c[i][j] = 0.0f;
		}
	}

	// Compute matrix multiplication.
	// C <- C + A x B
	for (int i = 0; i < N; ++i) 
	{
		for (int j = 0; j < N; ++j) 
		{
			for (int k = 0; k < N; ++k) 
			{
		        	c[i][j] = a[i][k] * b[k][j];
		    	}
		}
	}

	for (int i=0; i<N; ++i)
	{
		for(int j=0; j<N; ++j) cout<<a[i][j]<<" ";
		cout<<endl;
	}
	
	for (int i=0; i<N; ++i)
	{
		for(int j=0; j<N; ++j) cout<<b[i][j]<<" ";
		cout<<endl;
	}

	for (int i=0; i<N; ++i)
	{
		for(int j=0; j<N; ++j) cout<<c[i][j]<<" ";
		cout<<endl;
	}

    return 0;
}
